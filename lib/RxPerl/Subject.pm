package RxPerl::Subject;
use strict;
use warnings;

use base 'RxPerl::Observable';

our $VERSION = "v6.1.1";

# over-rideable
sub _on_subscribe {
    my ($self, $subscriber) = @_;
}

sub new {
    my ($class) = @_;

    my %subscribers;

    my $self; $self = $class->SUPER::new(sub {
        my ($subscriber) = @_;

        if ($self->{_closed}) {
            my ($type, @args) = @{ $self->{_closed} };
            $subscriber->{$type}->(@args) if defined $subscriber->{$type};
            return;
        }

        $subscribers{$subscriber} = $subscriber;
        $self->_on_subscribe($subscriber);

        return sub {
            delete $subscribers{$subscriber};
        };
    });

    $self->{_closed} = 0;
    foreach my $type (qw/ error complete /) {
        $self->{$type} = sub {
            return if $self->{_closed};
            $self->{_closed} = [$type, @_];
            foreach my $subscriber (values %subscribers) {
                $subscriber->{$type}->(@_) if defined $subscriber->{$type};
            }
            %subscribers = ();
            # TODO: maybe: delete @$self{qw/ next error complete /};
            # (Think about how subclasses such as BehaviorSubjects will be affected)
        };
    }
    $self->{next} = sub {
        foreach my $subscriber (values %subscribers) {
            $subscriber->{next}->(@_) if defined $subscriber->{next};
        }
    };

    return $self;
}

sub next {
    my $self = shift;

    $self->{next}->(splice @_, 0, 1) if defined $self->{next};
}

sub error {
    my $self = shift;

    $self->{error}->(splice @_, 0, 1) if defined $self->{error};
}

sub complete {
    my $self = shift;

    $self->{complete}->() if defined $self->{complete};
}

1;
